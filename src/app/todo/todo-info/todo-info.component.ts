

import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';

import { Observable } from 'rxjs';
import { TodoServiceService } from '../todo-service.service';



import { Todo } from '../todo.models';

@Component({
  selector: 'app-todo-info',
  templateUrl: './todo-info.component.html',
})
export class TodoInfoComponent {
  todos$!: Observable<Todo[]>;

constructor(private todoService: TodoServiceService ) {}


ngOnInit(): void {
  this.todos$ = this.todoService.todosChanged$;
}
  deleteAllTodos(): void {
    this.todoService.deleteAll();
    
  }

}